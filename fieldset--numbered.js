(($, Drupal) => {
  'use strict';

  Drupal.behaviors.fieldset_numbered = {
    attach: function(context, settings) {
      $('.fieldset--numbered', context).once('disableSubmitButton').on('focusin', function () {
        var $fieldset = $(this);
        resetFocusState($fieldset);
      });

      function resetFocusState($fieldset) {
        var $form = $fieldset.parents('form');
        $form.find('.fieldset--numbered').removeClass('in-focus');
        $fieldset.prevAll().addClass('in-focus');
        $fieldset.addClass('in-focus');
      }
    }
  };

})(jQuery, Drupal);
